var graph;
var timeseries_data = [];

var palette = new Rickshaw.Color.Palette( { scheme: "cool" } );

var init = false;

var max_series = 8;

var series = [];

var seriesHash = {};

var WINDOW_SIZE = 500;

var play = true;

$(document).ready(function() {

    var graph;

    var eventSource = new EventSource("/timeseries/");

    var hs=$('#slider').slider();

    eventSource.onmessage = function(e) {
        if(!play)
            return;

        var newData = jQuery.parseJSON( e.data );
        var date = new XDate(newData.polltime.substring(0,newData.polltime.length-4));
        var newPoint = {"x":date.getTime()/1000, "y":parseFloat(newData.data)};

        var id = newData.sensor;

        if("undefined" === typeof seriesHash[id] && series.length < max_series){
            seriesHash[id] = {
                color: palette.color(),
                data: [],
                name: id
            }
            series.push(seriesHash[id]);

            if(init) {
                $('#legend').empty();
                legend = new Rickshaw.Graph.Legend({
                    graph: graph,
                    element: document.getElementById('legend')
                });

                var shelving = new Rickshaw.Graph.Behavior.Series.Toggle( {
                    graph: graph,
                    legend: legend
                } );

                var highlighter = new Rickshaw.Graph.Behavior.Series.Highlight( {
                    graph: graph,
                    legend: legend
                } );
            }

        }

        if("undefined" !== typeof seriesHash[id]) {
            var d = seriesHash[id].data;
            while(d.length > WINDOW_SIZE)
                d.shift();
            d.push(newPoint);
        }

        if(!init){
            init = true;

            graph = new Rickshaw.Graph( {
                element: document.getElementById("graph"),
                renderer: 'line',
                series: series,
                min: "auto"
              });
            graph.render();

            var x_axis = new Rickshaw.Graph.Axis.Time({
              graph: graph
            });
            x_axis.graph.update();

            var yAxis = new Rickshaw.Graph.Axis.Y( {
                graph: graph,
                tickFormat: Rickshaw.Fixtures.Number.formatKMBT
            } );

            yAxis.render();

            var detail = new Rickshaw.Graph.HoverDetail({ 
                graph: graph,
                xFormatter: function(x) { return new XDate(x*1000).toString() },
                yFormatter: function(y) { return y}
            });

            var slider = new Rickshaw.Graph.RangeSlider({
                graph: graph,
                element: $('#slider')
            });

            var annotator = new Rickshaw.Graph.Annotate({
                graph: graph,
                element: document.getElementById('timeline')
            });

            var legend = new Rickshaw.Graph.Legend( {
                graph: graph,
                element: document.getElementById('legend')
            } );

            setInterval(function() {
                graph.update();
            }, 1000*3);

        }


    };

    eventSource.onerror = function(e) {
        console.debug(e);
    };

  Leap.loop({ enableGestures: true }, function(frame) {
      if(frame.pointables.length == 2) {
        var coeff = (graph.dataDomain()[1]-graph.dataDomain()[0])/150;
        var middle = graph.dataDomain()[0]+(graph.dataDomain()[1]-graph.dataDomain()[0])/2;

        var left = middle+(frame.pointables[1].tipPosition[0]*coeff);
        var right = middle+(frame.pointables[0].tipPosition[0]*coeff);

        hs.slider( 'option', 'values', [left, right]);
        hs.slider('option','slide')
           .call(hs,null,{ handle: $('.ui-slider-handle', hs), values: [left, right] });
        
        graph.update();
      }

      for(i=0, len=frame.gestures.length; i<len; i++){
        if(frame.gestures[i].type === 'swipe' && frame.gestures[i].state === 'stop'){
          console.debug("swipe");
        }
      }
  });

});

$(function() {
    $( "#play" ).button({
      text: false,
      icons: {
        primary: "ui-icon-play"
      }
    })
    .click(function() {
      var options;
      if ( $( this ).text() === "play" ) {
        options = {
          label: "pause",
          icons: {
            primary: "ui-icon-pause"
          }
        };
        play = false;
      } else {
        options = {
          label: "play",
          icons: {
            primary: "ui-icon-play"
          }
        };
        play = true;
      }
      $( this ).button( "option", options );
    });
});

var getClosestValue = function(a, x) {
    var lo = -1, hi = a.length;
    while (hi - lo > 1) {
        var mid = Math.round((lo + hi)/2);
        if (a[mid].x <= x) {
            lo = mid;
        } else {
            hi = mid;
        }
    }
    if (a[lo].x == x) hi = lo;
    return lo;
}
