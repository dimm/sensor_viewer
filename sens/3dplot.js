if (!Detector.webgl) Detector.addGetWebGLMessage();
var clock = new THREE.Clock();
var SCREEN_HEIGHT = window.innerHeight;
var SCREEN_WIDTH = window.innerWidth;
var stats;
var scatterPlot;
var camera, scene, renderer, mesh = [];
var projector;
var controls;

var selectedCubes = [];

init();
animate();

var normal_color = {h:0.3250825082508251, s:0.8632478632478633, v:0.4588235294117647};

var tempScale = chroma.scale(['FFFEA8', 'feb24c', 'f03b20']).domain([20, 65]);
tempScale.correctLightness(true);

function init() {
  renderer = new THREE.WebGLRenderer( { antialias: true } );
  renderer.setClearColor(0xEEEEEE, 1.0);
  renderer.setSize( window.innerWidth, window.innerHeight );

  projector = new THREE.Projector();

  container = document.getElementById( 'container' );
  container.appendChild( renderer.domElement );

  stats = new Stats();
  stats.domElement.style.position = 'absolute';
  stats.domElement.style.top = '0px';
  stats.domElement.style.zIndex = 100;
  container.appendChild( stats.domElement );


    scene = new THREE.Scene();

    camera = new THREE.PerspectiveCamera(36, SCREEN_WIDTH / SCREEN_HEIGHT, 1, 10000);
    camera.position.z = 135;
    camera.position.x = 135;
    camera.position.y = 95;
    scene.add(camera);


    // add subtle ambient lighting
    var ambientLight = new THREE.AmbientLight(0x222222);
    scene.add(ambientLight);

    // directional lighting
    var directionalLight = new THREE.DirectionalLight(0xffffff);
    directionalLight.position.set(1, 1, 1).normalize();
    scene.add(directionalLight);


    controls = new THREE.OrbitControls( camera);

    controls.rotateSpeed = 1.0;
    controls.zoomSpeed = 1.2;
    controls.panSpeed = 0.8;

    controls.noZoom = false;
    controls.noPan = false;

    controls.staticMoving = true;
    controls.dynamicDampingFactor = 0.3;

    controls.addEventListener( 'change', render );


    scatterPlot = new THREE.Object3D();
    scene.add(scatterPlot);

    scatterPlot.rotation.y = 0.5;
    function v(x, y, z) {
        return new THREE.Vector3(x, y, z);
    }

    var lineGeo = new THREE.Geometry();
    lineGeo.vertices.push(
            v(0, 0, 0), v(50, 0, 0),
            v(0, 0, 0), v(0, 50, 0),
            v(0, 0, 0), v(0, 0, 50),

            v(0, 50, 0), v(50, 50, 0),
            v(0, 0, 0), v(50, 0, 0),
            v(0, 50, 50), v(50, 50, 50),
            v(0, 0, 50), v(50, 0, 50),

            v(0, 0, 50), v(50, 0, 50),
            v(0, 0, 0), v(50, 0, 0),
            v(0, 50, 0), v(50, 50, 0),
            v(0, 0, 0), v(50, 0, 0),

            v(50, 0, 0), v(50, 50, 0),
            v(0, 0, 0), v(0, 50, 0),
            v(50, 0, 50), v(50, 50, 50),
            v(0, 0, 50), v(0, 50, 50),

            v(0, 0, 50), v(0, 50, 50),
            v(0, 0, 0), v(0, 50, 0),
            v(50, 0, 0), v(50, 50, 0),
            v(0, 0, 0), v(0, 50, 0),

            v(50, 50, 0), v(50, 50, 50),
            v(50, 0, 0), v(50, 0, 50),
            v(0, 50, 0), v(0, 50, 50),
            v(0, 0, 0), v(0, 0, 50),

            v(0, 0, 0), v(0, 0, 50),
            v(50, 0, 0), v(50, 0, 50),
            v(0, 50, 0), v(0, 50, 50),
            v(0, 0, 0), v(0, 0, 50)
    );
    var lineMat = new THREE.LineBasicMaterial({color:0x808080, lineWidth:1});
    var line = new THREE.Line(lineGeo, lineMat);
    line.type = THREE.Lines;
    scatterPlot.add(line);

    $.ajax({
      url: "sensors_loc.txt",
      success: function(data) {
        var pos = $.csv.toObjects(data);
        createMesh({vertices: pos}, scene)
        renderer.render(scene, camera);
        setTimeout(initEvents, 2);
        //setTimeout(initTemp, 2);
      },
      dataType: "text"
    });

    window.addEventListener('resize', onWindowResize, false);
    document.addEventListener( 'dblclick', onDocumentMouseClick, false );
    document.addEventListener( 'mousemove', onDocumentMouseMove, false );
}


function initEvents() {
    var eventSource = new EventSource("/timeseries_cp/");

    eventSource.onmessage = function(e) {
        var newData = jQuery.parseJSON( e.data );

	console.debug(newData);

        var cube = scene.getObjectByName(newData.sensor, true);

        if(typeof cube != "undefined") {
            cube.material.color.setHex( 0xff0000 );;

            new TWEEN.Tween(cube.material.color.getHSV())
                .to(normal_color, 150000)
                .easing(TWEEN.Easing.Quartic.Out)
                .onUpdate( function() {
                        THREE.ColorConverter.setHSV( cube.material.color, this.h, this.s, this.v );
                    }
                )
                .start();
        }

        $('#date').text(new XDate(newData.polltime*1000, true).toString());

    }
}

function initTemp() {
    var eventSource = new EventSource("/timeseries/");

    eventSource.onmessage = function(e) {
        var newData = jQuery.parseJSON( e.data );

        var cube = scene.getObjectByName(newData.sensor, true);

        if(typeof cube != "undefined") {
            cube.material.color.setHex( parseInt(tempScale(newData.data).hex().replace(/^#/, ''), 16) );
        }

    }
}

function createMesh(originalGeometry, scene) {
    var i, c;

    var vertices = originalGeometry.vertices;
    var vl = vertices.length;

    var geometry = new THREE.Geometry();
    var vertices_tmp = [];

    for (i = 0; i < vl; i++) {
        var cube = new THREE.Mesh(
            new THREE.CubeGeometry( 0.4, 0.6, 0.4 ), 
            new THREE.MeshLambertMaterial({color: 0x157510})
            );
        cube.position.x = vertices[i].x;
        cube.position.y = vertices[i].z;
        cube.position.z = vertices[i].y;
        cube.name = vertices[i].id;
        cube.type="sensor";
        scatterPlot.add(cube);

    }
}

function animate() {
    stats.update();
    controls.update();

    selectedCubes.forEach(function(cube) {
        cube.rotation.x += 0.05;
        cube.rotation.y += 0.05;
    });

    requestAnimationFrame(animate);
    render();
}

function render() {
  renderer.render( scene, camera );
  TWEEN.update();
}

function onWindowResize(event) {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize( window.innerWidth, window.innerHeight );

    // controls.handleResize();

    render();
}


function onDocumentMouseClick( event ) {

    event.preventDefault();

    var vector = new THREE.Vector3( ( event.clientX / window.innerWidth ) * 2 - 1, - ( event.clientY / window.innerHeight ) * 2 + 1, 0.5 );
    projector.unprojectVector( vector, camera );

    var raycaster = new THREE.Raycaster( camera.position, vector.sub( camera.position ).normalize() );

    var intersects = raycaster.intersectObjects( scene.children, true );

    if ( intersects.length > 0 && intersects[0].object.type == "sensor") {
        var obj = intersects[0].object;
        var index = selectedCubes.indexOf(obj);

        if (index > -1) {
            var cube = selectedCubes[index];
            cube.rotation.x = 0;
            cube.rotation.y = 0;
            selectedCubes.splice(index, 1);
        } else {
            selectedCubes.push(obj);
        }
    }
}

function onDocumentMouseMove( event ) {

    var vector = new THREE.Vector3( ( event.clientX / window.innerWidth ) * 2 - 1, - ( event.clientY / window.innerHeight ) * 2 + 1, 0.5 );
    projector.unprojectVector( vector, camera );

    var raycaster = new THREE.Raycaster( camera.position, vector.sub( camera.position ).normalize() );

    var intersects = raycaster.intersectObjects( scene.children, true );

    if ( intersects.length > 0 && intersects[0].object.type == "sensor") {
        var obj = intersects[0].object;
        $('#sens_info').text( "Sensor: "+obj.name + " ["+ obj.position.x+","+ obj.position.z+","+ obj.position.y+"]");
    }
}


THREE.Color.prototype.getHSV = function() { // http://stackoverflow.com/questions/12504449/how-to-tween-between-two-colours-using-three-js
    var rr, gg, bb,
        h, s,
        r = this.r,
        g = this.g,
        b = this.b,
        v = Math.max(r, g, b),
        diff = v - Math.min(r, g, b),
        diffc = function(c)
        {
            return (v - c) / 6 / diff + 1 / 2;
        };
    if (diff == 0) {
        h = s = 0;
    } else {
        s = diff / v;
        rr = diffc(r);
        gg = diffc(g);
        bb = diffc(b);

        if (r === v) {
            h = bb - gg;
        } else if (g === v) {
            h = (1 / 3) + rr - bb;
        } else if (b === v) {
            h = (2 / 3) + gg - rr;
        }
        if (h < 0) {
            h += 1;
        } else if (h > 1) {
            h -= 1;
        }
    }
    return {
        h: h,
        s: s,
        v: v
    };
};