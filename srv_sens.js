var fs = require("fs");
var csv = require("csv");

var sys = require('sys');
var http = require('http');
var static = require('node-static');

var http = require('http');
var url = require('url');

var file = new(static.Server)('.');

var rateLimit = require('function-rate-limit');

var Leap = require('leapjs');

var events = require('events');
var eventEmitter = new events.EventEmitter();

var emitter = rateLimit(1, 1000, function (event, data) {
    eventEmitter.emit(event, data);
});

var csv_reader = csv().from.stream(fs.createReadStream("data/out_changepoints"), { "columns" : ["cp", "polltime", "sensor"]})
.on("record", function(data){
	emitter('changepoint', data);
})
.on("end", function(){
    console.log("done");
})
.on("error", function(data){
    console.log(data);
});


require('http').createServer(function (req, res) {
 	var url = require('url').parse(req.url, true);

	var pathfile = url.pathname;

	if (pathfile.search(/^\/timeseries_cp/) == 0) { // changepoints
		res.writeHead(200, {'Content-Type': 'text/event-stream',
				    'Cache-Control': 'no-cache',
				    'Connection': 'Keep-Alive',
					'Access-Control-Allow-Origin': '*'});
		console.log("CP req!");

		var fn = function (data) {
		    res.write ('id: '+(new Date()).getTime()+'\n');
		    res.write ('data: '+JSON.stringify(data)+'\n\n');
		};

		eventEmitter.on('changepoint', fn);

		res.on('close', function() {
			console.log("disconnected ");
			eventEmitter.removeListener('changepoint', fn);
	    	return;
		});

		res.on('error', function(err) {
			console.log(err);
	    	return;
		});
		
	} else if (pathfile.search(/^\/timeseries/) == 0) {

		console.log("Timeseries req!");

		res.writeHead(200, {'Content-Type': 'text/event-stream',
				    'Cache-Control': 'no-cache',
				    'Connection': 'Keep-Alive',
					'Access-Control-Allow-Origin': '*'});

		var fn = rateLimit(10000, 1000, function (data) {
		    res.write ('id: '+(new Date()).getTime()+'\n');
		    res.write ('data: '+JSON.stringify(data)+'\n\n');
		});


		var csv_reader = csv().from.stream(fs.createReadStream("data/out"), { "columns" : ["sensor", "bus", "polltime", "data"]})
		.on("record", function(data){
			fn(data);
		})
		.on("end", function(){
		    console.log("done");
		})
		.on("error", function(data){
		    console.log(data);
		});

		res.on('close', function() {
			console.log("disconnected ");
			csv_reader.end();
	    	return;
		});

		res.on('error', function(err) {
			console.log(err);
			csv_reader.end();
	    	return;
		});

	} else {
		file.serve(req, res).
		on("error", function(error) {
			console.log(error);
		});
	}
}).listen(8080);
